# Unproblematic code
print("Hello world")

# SAST should find a problem with this code
try:
    do_something()
except:
    handle_exception()
